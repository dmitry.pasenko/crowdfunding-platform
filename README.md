# Crowdfunding Application
This application allows users to create campaigns and make donations using cryptocurrencies. The front-end is built with React and Material-UI, and the back-end is built with NestJS and Sequelize, using MySQL as the database.

## Prerequisites
Docker and Docker Compose installed on your system for running the back-end.
Node.js and npm installed for running the front-end locally.

## Getting Started
### Cloning the Repository
 
```bash
git clone https://gitlab.com/dmitry.pasenko/crowdfunding-platform
```

```bash
cd crowdfunding-app
```

### Running the Back-End with Docker Compose
The back-end, including the database, is containerized and can be run using Docker Compose.

Build and start the services:

```bash
docker-compose up --build
```

This command will build and start the services defined in the docker-compose.yml file. The back-end API will be available at http://localhost:4000.

To stop the services, run:

```bash
docker-compose down
```

### Running the Front-End Locally
To run the front-end locally, follow these steps:

Navigate to the front-end directory:

```bash
cd frontend
```

Install dependencies:

```bash
npm install
```

Start the development server:

```bash
npm start
```
The front-end will be available at http://localhost:3000.

## Available API Endpoints
The back-end API provides several endpoints to interact with the crowdfunding application.

### Authentication
**Generate Token**
```bash
GET /api/auth/generateToken
```
Generates a JWT token for authentication.
**It should be stored in localStorage under api-key key!!!**

### Campaigns
**Get All Campaigns**
```bash
GET /api/campaigns
```
Retrieves all active campaigns.

**Mark Campaign as Fraud**
```bash
POST /api/campaigns/markAsFraud
```
Marks a campaign as fraud.
Body Parameters:
* uuid (string): The UUID of the campaign to be marked as fraud.

### Donations
**Create Donation**
```bash
POST /api/donations
```
Creates a new donation for a campaign.
Body Parameters:
amount (number): The amount of the donation in USD.
* cryptoCurrencyId (number): The ID of the cryptocurrency used for the donation.
* cryptoAmount (number): The amount of the cryptocurrency.
* campaignId (string): The UUID of the campaign.

### Get User Donations
```bash
GET /api/donations
```
Retrieves all donations made by the authenticated user.

### Crypto Currencies
**Get All Crypto Currencies**
```bash
GET /api/crypto-currencies
```
Retrieves all supported cryptocurrencies with their exchange rates.

###Environment Variables
The application uses the following environment variables, set in the docker-compose.yml file or directly in the environment:

**MySQL Service:**
* MYSQL_ROOT_PASSWORD: The root password for the MySQL database.
* MYSQL_DATABASE: The name of the database to create and use.
* MYSQL_USER: The MySQL user name.
* MYSQL_PASSWORD: The MySQL user password.

**NestJS Service:**

* DB_HOST: The host name of the MySQL database (set to mysql for Docker networking).
* DB_PORT: The port on which MySQL is running (default 3306).
* DB_USERNAME: The MySQL user name.
* DB_PASSWORD: The MySQL user password.
* DB_DATABASE: The name of the MySQL database.
* JWT_SECRET: A secret key for JWT token generation.

###Database Initialization
On the first run, the MySQL service will initialize the database with tables and seed data from SQL scripts located in the init_db directory.

### Project Structure
* backend: The NestJS back-end API.
* backend/init_db: SQL scripts for initializing the MySQL database.
* frontend: The React front-end application.

### Troubleshooting
* Ensure Docker and Docker Compose are installed and running for the back-end.
* Ensure Node.js and npm are installed for running the front-end locally.
* Check the logs for any errors using docker-compose logs for the back-end and the console output for the front-end.
* Verify the environment variables are correctly set in the docker-compose.yml file for the back-end.
