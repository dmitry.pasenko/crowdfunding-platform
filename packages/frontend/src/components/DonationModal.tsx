import React, { useEffect, useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  MenuItem,
  Button,
  Typography,
} from "@mui/material";
import { createDonation, fetchCryptoCurrencies } from "../api"; // Функция для отправки доната на сервер
import { Campaign } from "../types/Campaign";
import { CryptoCurrency } from "../types/CryptoCurrency"; // Тип данных для Campaign

interface DonationModalProps {
  open: boolean;
  onClose: () => void;
  campaign: Campaign | null;
  onDonationSuccess: () => void;
}

const DonationModal: React.FC<DonationModalProps> = ({
  open,
  onClose,
  campaign,
  onDonationSuccess,
}) => {
  const [donationAmount, setDonationAmount] = useState<number>(10);
  const [cryptoCurrencyId, setCryptoCurrencyId] = useState<number | null>(null);
  const [cryptoCurrencies, setCryptoCurrencies] = useState<CryptoCurrency[]>([]);
  const [submitError, setSubmitError] = useState<string | null>(null);

  useEffect(() => {
    const loadCryptoCurrencies = async (): Promise<void> => {
      try {
        const data = await fetchCryptoCurrencies();
        if (data) {
          setCryptoCurrencies(data);
          setCryptoCurrencyId(data[0]?.id || null);
        }
      } catch (error) {
        setSubmitError("Failed to load crypto currencies.");
      }
    };

    loadCryptoCurrencies();
  }, []);

  const handleSubmit = async (): Promise<void> => {
    if (!campaign || cryptoCurrencyId === null) return;

    try {
      await createDonation({
        cryptoAmount: donationAmount,
        cryptoCurrencyId,
        campaignId: campaign.id,
      });

      onDonationSuccess();
      handleClose();
    } catch (error) {
      setSubmitError("Failed to submit donation. Please try again.");
    }
  };

  const handleClose = (): void => {
    onClose();
    setDonationAmount(0);
    setCryptoCurrencyId(cryptoCurrencies[0]?.id || null);
    setSubmitError(null);
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Donate to {campaign?.title}</DialogTitle>
      <DialogContent>
        <TextField
          label="Amount"
          type="number"
          fullWidth
          margin="normal"
          value={donationAmount}
          onChange={(e) => setDonationAmount(Number(e.target.value))}
        />
        <TextField
          label="Crypto Currency"
          select
          fullWidth
          margin="normal"
          value={cryptoCurrencyId || ""}
          onChange={(e) => setCryptoCurrencyId(Number(e.target.value))}
        >
          {cryptoCurrencies.map((currency) => (
            <MenuItem key={currency.id} value={currency.id}>
              {currency.name} ({currency.symbol})
            </MenuItem>
          ))}
        </TextField>
        {submitError && (
          <Typography color="error" variant="body2">
            {submitError}
          </Typography>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Cancel
        </Button>
        <Button onClick={handleSubmit} color="primary">
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DonationModal;
