import React from "react";
import { AppBar, Toolbar, Typography, Button } from "@mui/material";
import { Link } from "react-router-dom";

const Navbar: React.FC = () => {
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Crowdfunding App
        </Typography>
        <Button color="inherit" component={Link} to="/">
          Campaigns List
        </Button>
        <Button color="inherit" component={Link} to="/my-donations">
          My Donations
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
