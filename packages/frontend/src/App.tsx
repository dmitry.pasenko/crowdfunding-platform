import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import CampaignsList from "./pages/CampaignsList";
import MyDonations from "./pages/MyDonations";
import { Container } from "@mui/material";

function App(): React.ReactElement {
  return (
    <Router>
      <Navbar />
      <Container>
        <Routes>
          <Route path="/" element={<CampaignsList />} />
          <Route path="/my-donations" element={<MyDonations />} />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;
