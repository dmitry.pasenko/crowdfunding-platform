import axios from "axios";
import axiosInstance from "./axiosInstance";
import { Campaign } from "./types/Campaign";
import { Donation } from "./types/Donation";
import { CryptoCurrency } from "./types/CryptoCurrency";

const handleApiError = (error: unknown): never => {
  if (axios.isAxiosError(error)) {
    if (error.response?.status === 401) {
      throw new Error("Unauthorized access. Please log in.");
    } else if (error.response?.status === 403) {
      throw new Error("Forbidden access.");
    } else if (error.response?.status === 404) {
      throw new Error("Resource not found.");
    } else {
      throw new Error("Server error. Please try again later.");
    }
  } else {
    throw new Error("Network error. Please check your connection.");
  }
};

const apiGet = async <T>(endpoint: string): Promise<T | undefined> => {
  try {
    const response = await axiosInstance.get<T>(endpoint);
    return response.data;
  } catch (error) {
    handleApiError(error);
    return undefined;
  }
};

const apiPost = async <T, U>(endpoint: string, data: U): Promise<T | undefined> => {
  try {
    const response = await axiosInstance.post<T>(endpoint, data);
    return response.data;
  } catch (error) {
    handleApiError(error);
    return undefined;
  }
};

export const fetchCampaigns = async (): Promise<Campaign[] | undefined> => {
  return apiGet<Campaign[]>("/api/campaigns");
};

export const fetchDonations = async (): Promise<Donation[] | undefined> => {
  return apiGet<Donation[]>("/api/donations");
};

export const fetchCryptoCurrencies = async (): Promise<CryptoCurrency[] | undefined> => {
  return apiGet<CryptoCurrency[]>("/api/crypto-currencies");
};

export const createDonation = async (donationData: {
  cryptoAmount: number;
  cryptoCurrencyId: number;
  campaignId: string;
}): Promise<Donation | undefined> => {
  return apiPost<Donation, typeof donationData>("/api/donations", donationData);
};
