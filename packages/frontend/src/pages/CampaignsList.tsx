import React, { useEffect, useState } from "react";
import { Typography, Card, CardContent, Grid, Button } from "@mui/material";
import { Campaign } from "../types/Campaign";
import { fetchCampaigns } from "../api";
import { PageTemplate } from "../layouts/PageTemplate";
import DonationModal from "../components/DonationModal";

const CampaignsList: React.FC = () => {
  const [campaigns, setCampaigns] = useState<Campaign[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [selectedCampaign, setSelectedCampaign] = useState<Campaign | null>(null);

  const loadCampaigns = async (): Promise<void> => {
    setLoading(true);
    try {
      const data: Campaign[] = (await fetchCampaigns()) || [];
      setCampaigns(data);
    } catch (error) {
      setError("Failed to load campaigns");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    loadCampaigns();
  }, []);

  const handleOpen = (campaign: Campaign): void => {
    setSelectedCampaign(campaign);
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
    setSelectedCampaign(null);
  };

  const handleDonationSuccess = (): void => {
    loadCampaigns();
  };

  if (loading) {
    return <Typography variant="h6">Loading campaigns...</Typography>;
  }

  if (error) {
    return <Typography variant="h6">{error}</Typography>;
  }

  if (campaigns.length === 0) {
    return <Typography variant="h6">No campaigns found.</Typography>;
  }

  return (
    <PageTemplate title={"Campaigns List"}>
      <Grid container spacing={2}>
        {campaigns.map((campaign) => (
          <Grid item xs={12} sm={6} md={4} key={campaign.id}>
            <Card>
              <CardContent>
                <Typography variant="h5">{campaign.title}</Typography>
                <Typography variant="body1">{campaign.description}</Typography>
                <Typography variant="body2">
                  Goal: ${campaign.goalAmount} - Raised: ${campaign.raised}
                </Typography>
                <Typography variant="body2">Status: {campaign.status}</Typography>
                <Button variant="contained" color="primary" onClick={() => handleOpen(campaign)}>
                  Donate
                </Button>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
      <DonationModal
        open={open}
        onClose={handleClose}
        campaign={selectedCampaign}
        onDonationSuccess={handleDonationSuccess}
      />
    </PageTemplate>
  );
};

export default CampaignsList;
