import React, { useEffect, useState } from "react";
import { Typography, Grid, Card, CardContent } from "@mui/material";
import { Donation } from "../types/Donation";
import { fetchDonations } from "../api";
import { PageTemplate } from "../layouts/PageTemplate";

const MyDonations: React.FC = () => {
  const [donations, setDonations] = useState<Donation[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadDonations = async (): Promise<void> => {
      try {
        const data: Donation[] = (await fetchDonations()) || [];
        setDonations(data);
      } catch (error) {
        if (error instanceof Error) {
          setError(error.message);
        } else {
          setError("An unexpected error occurred.");
        }
      } finally {
        setLoading(false);
      }
    };

    loadDonations();
  }, []);

  if (loading) {
    return <Typography variant="h6">Loading donations...</Typography>;
  }

  if (error) {
    return <Typography variant="h6">{error}</Typography>;
  }

  if (donations.length === 0) {
    return <Typography variant="h6">No donations found.</Typography>;
  }

  return (
    <PageTemplate title={" My Donations"}>
      <Grid container spacing={2}>
        {donations.map((donation) => (
          <Grid item xs={12} sm={6} md={4} key={donation.id}>
            <Card>
              <CardContent>
                <Typography variant="h5"> {donation.campaign.title}</Typography>
                <Typography variant="body1">Amount: ${donation.amount}</Typography>
                <Typography variant="body2">
                  Date: {new Date(donation.createdAt).toLocaleDateString()}
                </Typography>
                <Typography variant="body2">State: {donation.state}</Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </PageTemplate>
  );
};

export default MyDonations;
