export interface CryptoCurrency {
  id: number;
  name: string;
  symbol: string;
  exchangeRate: number;
}
