export enum CampaignStatus {
  ACTIVE = "active",
  EXPIRED = "expired",
  FRAUD = "fraud",
  SUCCESSFUL = "successful",
}

export interface Campaign {
  id: string;
  title: string;
  description: string;
  goalAmount: number;
  raised: number;
  status: CampaignStatus;
}
