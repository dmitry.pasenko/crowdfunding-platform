import { Campaign } from "./Campaign";

export enum DonationStatus {
  VALID = "valid",
  INVALID = "invalid",
}

export interface Donation {
  id: string;
  campaign: Campaign;
  amount: number;
  createdAt: string;
  state: DonationStatus;
}
