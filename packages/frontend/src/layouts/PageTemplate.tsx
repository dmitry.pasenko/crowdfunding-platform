import React, { ReactNode } from "react";
import { Box, Typography } from "@mui/material";

interface PageTemplateProps {
  title: string;
  children: ReactNode;
}

export const PageTemplate: React.FC<PageTemplateProps> = (props: PageTemplateProps) => {
  return (
    <>
      <Box sx={{ mt: 10, mb: 10 }}>
        <Typography variant="h5" component="h1" sx={{ pb: 5 }}>
          {props.title}
        </Typography>
        {props.children}
      </Box>
    </>
  );
};
