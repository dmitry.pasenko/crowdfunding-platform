import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreateDonationDto {
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  cryptoAmount: number;

  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  campaignId: number;

  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  cryptoCurrencyId: number;
}
