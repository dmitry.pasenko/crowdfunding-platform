import {
  Body,
  Controller,
  Get,
  Logger,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { DonationService } from './donation.service';
import { CreateDonationDto } from './dto/create-donation.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('api/donations')
@UseGuards(AuthGuard('jwt'))
export class DonationController {
  constructor(private readonly donationService: DonationService) {}
  private readonly logger = new Logger(DonationController.name);

  @Post()
  async createDonation(
    @Body() createDonationDto: CreateDonationDto,
    @Request() req,
  ) {
    const userId = req.user.userId; //user ID from JWT token
    try {
      return await this.donationService.createDonation(
        createDonationDto,
        userId,
      );
    } catch (error) {
      this.logger.log('Error creating donation', error);
    }
  }

  @Get()
  async getMyDonations(@Request() req) {
    const userId = req.user.userId; //user ID from JWT token
    try {
      return await this.donationService.getDonationsByUser(userId);
    } catch (error) {
      this.logger.log('Unable to fetch donations', error);
    }
  }
}
