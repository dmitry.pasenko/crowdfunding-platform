import { Module } from '@nestjs/common';
import { DonationService } from './donation.service';
import { DonationController } from './donation.controller';
import { CoreModule } from '../core/core.module';

@Module({
  imports: [CoreModule],
  providers: [DonationService],
  exports: [DonationService],
  controllers: [DonationController],
})
export class DonationModule {}
