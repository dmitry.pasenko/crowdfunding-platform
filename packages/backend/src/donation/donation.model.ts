import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
  DataType,
} from 'sequelize-typescript';
import { Campaign } from '../campaign/campaign.model';
import { User } from '../user/user.model';
import { CryptoCurrency } from '../crypto-currency/crypto-currency.model';

@Table
export class Donation extends Model<Donation> {
  @Column({
    type: DataType.DECIMAL(10, 2),
    allowNull: false,
  })
  amount: number;

  @Column({
    type: DataType.DECIMAL(10, 6),
    allowNull: false,
  })
  cryptoAmount: number;

  @Column({
    type: DataType.ENUM('valid', 'invalid'),
    allowNull: false,
    defaultValue: 'valid',
  })
  state: string;

  @ForeignKey(() => Campaign)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  campaignId: number;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  userId: number;

  @ForeignKey(() => CryptoCurrency)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  cryptoCurrencyId: number;

  @BelongsTo(() => Campaign)
  campaign: Campaign;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => CryptoCurrency)
  cryptoCurrency: CryptoCurrency;
}
