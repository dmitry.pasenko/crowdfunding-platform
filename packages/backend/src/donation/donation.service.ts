import { Injectable, HttpException, HttpStatus, Inject } from '@nestjs/common';
import { Donation } from './donation.model';
import { Campaign } from '../campaign/campaign.model';
import { CreateDonationDto } from './dto/create-donation.dto';
import {
  CAMPAIGN_REPOSITORY,
  CampaignStatus,
  CRYPTO_CURRENCY_REPOSITORY,
  DONATION_REPOSITORY,
  DonationStatus,
} from '../core/constants';
import { CryptoCurrency } from '../crypto-currency/crypto-currency.model';

@Injectable()
export class DonationService {
  constructor(
    @Inject(CAMPAIGN_REPOSITORY)
    private readonly campaignRepository: typeof Campaign,
    @Inject(DONATION_REPOSITORY)
    private readonly donationRepository: typeof Donation,
    @Inject(CRYPTO_CURRENCY_REPOSITORY)
    private readonly cryptoCurrencyRepository: typeof CryptoCurrency,
  ) {}

  async createDonation(
    createDonationDto: CreateDonationDto,
    userId: number,
  ): Promise<Donation> {
    const { cryptoAmount, cryptoCurrencyId, campaignId } = createDonationDto;

    const campaign = await this.campaignRepository.findOne({
      where: { id: campaignId },
    });

    if (!campaign || campaign.status !== CampaignStatus.ACTIVE) {
      throw new HttpException(
        'Campaign is not active or does not exist',
        HttpStatus.BAD_REQUEST,
      );
    }

    const cryptoCurrency = await this.cryptoCurrencyRepository.findOne({
      where: { id: cryptoCurrencyId },
    });

    if (!cryptoCurrency) {
      throw new HttpException(
        'Crypto currency does not exist',
        HttpStatus.BAD_REQUEST,
      );
    }

    const usdAmount = cryptoAmount * cryptoCurrency.exchangeRate;

    const donation = new this.donationRepository({
      amount: usdAmount,
      cryptoAmount,
      state: DonationStatus.VALID,
      campaignId: campaign.id,
      userId,
      cryptoCurrencyId,
    });

    await donation.save();

    const raised = parseFloat(campaign.raised.toString());
    const totalRaised = raised + usdAmount;

    campaign.raised = parseFloat(totalRaised.toFixed(2));

    if (campaign.raised >= campaign.goalAmount) {
      campaign.status = CampaignStatus.SUCCESSFUL;
    }

    await campaign.save();

    return donation;
  }

  async getDonationsByUser(userId: number): Promise<Donation[]> {
    return this.donationRepository.findAll({
      where: { userId },
      include: [
        {
          model: Campaign,
          attributes: ['title'],
        },
      ],
    });
  }
}
