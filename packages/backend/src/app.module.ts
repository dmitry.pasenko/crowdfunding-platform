import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { User } from './user/user.model';
import { Campaign } from './campaign/campaign.model';
import { Donation } from './donation/donation.model';
import { CampaignModule } from './campaign/campaign.module';
import { DonationModule } from './donation/donation.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CryptoCurrency } from './crypto-currency/crypto-currency.model';
import { CoreModule } from './core/core.module';
import { CryptoCurrencyModule } from './crypto-currency/crypto-currency.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    SequelizeModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (cfg: ConfigService) => ({
        dialect: 'mysql',
        host: cfg.get('DB_HOST'),
        port: cfg.get('DB_PORT') as unknown as number,
        database: cfg.get('DB_DATABASE'),
        username: cfg.get('DB_USERNAME'),
        password: cfg.get('DB_PASSWORD'),
        models: [User, Campaign, Donation, CryptoCurrency],
      }),
      imports: undefined,
    }),
    CoreModule,
    AuthModule,
    CampaignModule,
    DonationModule,
    CryptoCurrencyModule,
  ],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
