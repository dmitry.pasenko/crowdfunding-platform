import { Table, Column, Model, DataType, HasMany } from 'sequelize-typescript';
import { Donation } from '../donation/donation.model';

@Table
export class CryptoCurrency extends Model<CryptoCurrency> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  symbol: string;

  @Column({
    type: DataType.FLOAT,
    allowNull: false,
  })
  exchangeRate: number; // in USD

  @HasMany(() => Donation)
  donations: Donation[];
}
