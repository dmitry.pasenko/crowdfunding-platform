import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { CryptoCurrency } from './crypto-currency.model';
import { CRYPTO_CURRENCY_REPOSITORY } from '../core/constants';

@Injectable()
export class CryptoCurrencyService {
  constructor(
    @Inject(CRYPTO_CURRENCY_REPOSITORY)
    private readonly cryptoCurrencyRepository: typeof CryptoCurrency,
  ) {}

  async findAll(): Promise<CryptoCurrency[]> {
    try {
      return this.cryptoCurrencyRepository.findAll<CryptoCurrency>();
    } catch (e) {
      throw new HttpException(
        'Failed to fetch crypto currencies',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
