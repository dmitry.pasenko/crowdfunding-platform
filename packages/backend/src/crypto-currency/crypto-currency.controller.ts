import { Controller, Get, Logger } from '@nestjs/common';
import { CryptoCurrencyService } from './crypto-currency.service';

@Controller('api/crypto-currencies')
export class CryptoCurrencyController {
  private readonly logger = new Logger(CryptoCurrencyController.name);

  constructor(private readonly cryptoCurrencyService: CryptoCurrencyService) {}

  @Get()
  async findAll() {
    try {
      return await this.cryptoCurrencyService.findAll();
    } catch (error) {
      this.logger.log(error);
    }
  }
}
