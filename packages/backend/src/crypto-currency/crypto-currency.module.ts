import { Module } from '@nestjs/common';
import { CryptoCurrencyController } from './crypto-currency.controller';
import { CoreModule } from '../core/core.module';
import { CryptoCurrencyService } from './crypto-currency.service';

@Module({
  imports: [CoreModule],
  providers: [CryptoCurrencyService],
  controllers: [CryptoCurrencyController],
})
export class CryptoCurrencyModule {}
