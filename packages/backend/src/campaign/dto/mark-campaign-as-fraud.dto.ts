import { IsNotEmpty, IsString } from 'class-validator';

export class MarkCampaignAsFraudDto {
  @IsNotEmpty()
  @IsString()
  uuid: string;
}
