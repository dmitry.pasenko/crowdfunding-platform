import {
  Table,
  Column,
  Model,
  DataType,
  BelongsTo,
  ForeignKey,
  HasMany,
  Default,
} from 'sequelize-typescript';
import { User } from '../user/user.model';
import { Donation } from '../donation/donation.model';

@Table
export class Campaign extends Model<Campaign> {
  @Default(DataType.UUIDV4) // not a primary key - just for demo purposes
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  uuid: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  title: string;

  @Column({
    type: DataType.TEXT,
    allowNull: false,
  })
  description: string;

  @Column({
    type: DataType.DECIMAL(20, 2),
    allowNull: false,
  })
  goalAmount: number;

  @Column({
    type: DataType.DECIMAL(20, 2),
    defaultValue: 0,
    allowNull: false,
  })
  raised: number;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  expirationDate: Date;

  @Column({
    type: DataType.ENUM('active', 'expired', 'fraud', 'successful'),
    allowNull: false,
    defaultValue: 'active',
  })
  status: string;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  campaignOwnerId: number;

  @BelongsTo(() => User)
  campaignOwner: User;

  @HasMany(() => Donation)
  donations: Donation[];
}
