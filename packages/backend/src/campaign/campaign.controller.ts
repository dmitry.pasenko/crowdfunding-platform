import { Controller, Post, Body, Get, Logger } from '@nestjs/common';
import { CampaignService } from './campaign.service';
import { MarkCampaignAsFraudDto } from './dto/mark-campaign-as-fraud.dto';

@Controller('api/campaigns')
export class CampaignController {
  private readonly logger = new Logger(CampaignController.name);
  constructor(private readonly campaignService: CampaignService) {}

  @Get()
  async findAll() {
    try {
      return await this.campaignService.findAll();
    } catch (error) {
      this.logger.error('Error fetching campaigns', error);
    }
  }

  @Post('markAsFraud')
  async markAsFraud(
    @Body() markCampaignAsFraudDto: MarkCampaignAsFraudDto,
  ): Promise<void> {
    try {
      await this.campaignService.markAsFraud(markCampaignAsFraudDto.uuid);
    } catch (error) {
      this.logger.error(
        `Error marking campaign as fraud: ${markCampaignAsFraudDto.uuid}`,
        error,
      );
    }
  }
}
