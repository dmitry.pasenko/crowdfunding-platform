import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Campaign } from './campaign.model';
import { User } from '../user/user.model';
import { Donation } from '../donation/donation.model';
import {
  CAMPAIGN_REPOSITORY,
  CampaignStatus,
  DONATION_REPOSITORY,
  DonationStatus,
  USER_REPOSITORY,
} from '../core/constants';

@Injectable()
export class CampaignService {
  constructor(
    @Inject(CAMPAIGN_REPOSITORY)
    private readonly campaignRepository: typeof Campaign,
    @Inject(USER_REPOSITORY)
    private readonly campaignOwnerRepository: typeof User,
    @Inject(DONATION_REPOSITORY)
    private readonly donationRepository: typeof Donation,
  ) {}

  async findAll(): Promise<Campaign[]> {
    try {
      const campaigns = await this.campaignRepository.findAll({
        include: [{ model: this.campaignOwnerRepository }],
      });

      if (campaigns.length === 0) {
        throw new HttpException('No campaigns found', HttpStatus.NOT_FOUND);
      }
      return campaigns;
    } catch (error) {
      throw new HttpException(
        'Failed to fetch campaigns',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async markAsFraud(uuid: string): Promise<void> {
    const campaign = await this.campaignRepository.findOne({
      where: { uuid },
      include: [User, Donation],
    });

    if (!campaign) {
      throw new NotFoundException(`Campaign with UUID ${uuid} not found`);
    }

    await this.campaignOwnerRepository.update(
      { isFraud: true },
      { where: { id: campaign.campaignOwnerId } },
    );

    await this.campaignRepository.update(
      { status: CampaignStatus.FRAUD },
      { where: { campaignOwnerId: campaign.campaignOwnerId } },
    );

    await this.donationRepository.update(
      { state: DonationStatus.INVALID },
      { where: { campaignId: campaign.id } },
    );
  }
}
