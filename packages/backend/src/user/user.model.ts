import {
  Column,
  DataType,
  DefaultScope,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';
import { Campaign } from '../campaign/campaign.model';
import { Donation } from '../donation/donation.model';

@DefaultScope(() => ({
  attributes: { exclude: ['password'] },
}))
@Table
export class User extends Model<User> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  })
  username: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  })
  email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  cryptoWalletAddress: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  })
  isFraud: boolean;

  @Column({
    type: DataType.ENUM('Donator', 'CampaignOwner', 'Both'),
    allowNull: false,
  })
  role: 'Donator' | 'CampaignOwner' | 'Both';

  @HasMany(() => Campaign, 'campaignOwnerId')
  campaigns: Campaign[];

  @HasMany(() => Donation)
  donations: Donation[];
}
