import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  async generateTestToken(): Promise<string> {
    const payload = { sub: '4', username: 'Donator 1' };
    return this.jwtService.sign(payload, { noTimestamp: true });
  }
}
