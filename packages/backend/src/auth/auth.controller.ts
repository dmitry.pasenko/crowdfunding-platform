import { Controller, Get } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('generateToken')
  async generateTestToken(): Promise<{ token: string }> {
    const token = await this.authService.generateTestToken();
    return { token };
  }
}
