import { Module } from '@nestjs/common';
import { Campaign } from '../campaign/campaign.model';
import { User } from '../user/user.model';
import { Donation } from '../donation/donation.model';
import { CryptoCurrency } from '../crypto-currency/crypto-currency.model';
import {
  CAMPAIGN_REPOSITORY,
  USER_REPOSITORY,
  DONATION_REPOSITORY,
  CRYPTO_CURRENCY_REPOSITORY,
} from './constants';

@Module({
  providers: [
    {
      provide: CAMPAIGN_REPOSITORY,
      useValue: Campaign,
    },
    {
      provide: USER_REPOSITORY,
      useValue: User,
    },
    {
      provide: DONATION_REPOSITORY,
      useValue: Donation,
    },
    {
      provide: CRYPTO_CURRENCY_REPOSITORY,
      useValue: CryptoCurrency,
    },
  ],
  exports: [
    CAMPAIGN_REPOSITORY,
    USER_REPOSITORY,
    DONATION_REPOSITORY,
    CRYPTO_CURRENCY_REPOSITORY,
  ],
})
export class CoreModule {}
