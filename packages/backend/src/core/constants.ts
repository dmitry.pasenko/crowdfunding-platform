export const USER_REPOSITORY = 'USER_REPOSITORY';
export const DONATION_REPOSITORY = 'DONATION_REPOSITORY';
export const CAMPAIGN_REPOSITORY = 'CAMPAIGN_REPOSITORY';
export const CRYPTO_CURRENCY_REPOSITORY = 'CRYPTO_CURRENCY_REPOSITORY';

export enum CampaignStatus {
  ACTIVE = 'active',
  EXPIRED = 'expired',
  FRAUD = 'fraud',
  SUCCESSFUL = 'successful',
}

export enum DonationStatus {
  VALID = 'valid',
  INVALID = 'invalid',
}
