DELIMITER //

CREATE PROCEDURE MarkExpiredCampaigns()
BEGIN
  UPDATE Campaigns
  SET status = 'expired'
  WHERE expirationDate <= NOW()
    AND status = 'active';
END //

DELIMITER ;
