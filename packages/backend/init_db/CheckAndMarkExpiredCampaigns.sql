CREATE EVENT IF NOT EXISTS CheckAndMarkExpiredCampaigns
ON SCHEDULE EVERY 10 SECOND
DO
  CALL MarkExpiredCampaigns();
