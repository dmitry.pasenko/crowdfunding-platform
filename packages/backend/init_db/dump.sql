DROP TABLE IF EXISTS Donations;
DROP TABLE IF EXISTS Campaigns;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS CryptoCurrencies;

CREATE TABLE IF NOT EXISTS Users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    cryptoWalletAddress VARCHAR(255),
    isFraud BOOLEAN NOT NULL DEFAULT FALSE,
    role ENUM('Donator', 'CampaignOwner', 'Both') NOT NULL,
    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO Users (username, email, password, cryptoWalletAddress, role) VALUES
('Owner 1', 'owner1@example.com', 'password1', '0x123...abc', 'CampaignOwner'),
('Owner 2', 'owner2@example.com', 'password2', '0x456...def', 'CampaignOwner'),
('Owner 3', 'owner3@example.com', 'password3', '0x789...ghi', 'CampaignOwner'),
('Donator 1', 'donator@example.com', 'password4', '0x111...jkl', 'Donator');

CREATE TABLE IF NOT EXISTS Campaigns (
    id INT AUTO_INCREMENT PRIMARY KEY,
     uuid CHAR(36) NOT NULL DEFAULT (UUID()),
    title VARCHAR(255) NOT NULL,
    description TEXT,
    goalAmount DECIMAL(20, 2) NOT NULL,
    raised DECIMAL(20, 2) NOT NULL DEFAULT 0,
    expirationDate DATETIME NOT NULL,
    status ENUM('active', 'expired', 'fraud', 'successful') NOT NULL DEFAULT 'active',
    campaignOwnerId INT,
    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (campaignOwnerId) REFERENCES Users(id)
);

INSERT INTO Campaigns (title, description, goalAmount, expirationDate, status, campaignOwnerId) VALUES
('Campaign 1', 'Description 1', 1000, NOW() + INTERVAL 1 YEAR, 'active', 1),
('Campaign 2', 'Description 2', 2000, NOW() + INTERVAL 2 YEAR, 'active', 1),
('Campaign 3', 'Description 3', 3000, NOW() + INTERVAL 3 MINUTE, 'active', 2),
('Campaign 4', 'Description 4', 4000, NOW() + INTERVAL 4 MINUTE, 'active', 2),
('Campaign 5', 'Description 5', 5000, NOW() + INTERVAL 5 MINUTE, 'active', 3),
('Campaign 6', 'Description 6', 6000, NOW() + INTERVAL 6 MINUTE, 'active', 3),
('Campaign 7', 'Description 7', 7000, NOW() + INTERVAL 7 MINUTE, 'active', 1),
('Campaign 8', 'Description 8', 8000, NOW() + INTERVAL 8 MINUTE, 'active', 2),
('Campaign 9', 'Description 9', 9000, NOW() + INTERVAL 9 MINUTE, 'active', 3),
('Campaign 10', 'Description 10', 10000, NOW() + INTERVAL 10 MINUTE, 'active', 1),
('Campaign 11', 'Description 11', 11000, NOW() + INTERVAL 11 MINUTE, 'active', 2),
('Campaign 12', 'Description 12', 12000, NOW() + INTERVAL 12 MINUTE, 'active', 3),
('Campaign 13', 'Description 13', 13000, NOW() + INTERVAL 13 MINUTE, 'active', 1),
('Campaign 14', 'Description 14', 14000, NOW() + INTERVAL 14 MINUTE, 'active', 2),
('Campaign 15', 'Description 15', 15000, NOW() + INTERVAL 15 MINUTE, 'active', 3),
('Campaign 16', 'Description 16', 16000, NOW() + INTERVAL 16 MINUTE, 'active', 1),
('Campaign 17', 'Description 17', 17000, NOW() + INTERVAL 17 MINUTE, 'active', 2),
('Campaign 18', 'Description 18', 18000, NOW() + INTERVAL 18 MINUTE, 'active', 3),
('Campaign 19', 'Description 19', 19000, NOW() + INTERVAL 19 MINUTE, 'active', 1),
('Campaign 20', 'Description 20', 20000, NOW() + INTERVAL 20 MINUTE, 'active', 2),
('Campaign 21', 'Description 21', 21000, NOW() + INTERVAL 21 MINUTE, 'active', 3),
('Campaign 22', 'Description 22', 22000, NOW() + INTERVAL 22 MINUTE, 'active', 1),
('Campaign 23', 'Description 23', 23000, NOW() + INTERVAL 23 MINUTE, 'active', 2),
('Campaign 24', 'Description 24', 24000, NOW() + INTERVAL 24 MINUTE, 'active', 3),
('Campaign 25', 'Description 25', 25000, NOW() + INTERVAL 25 MINUTE, 'active', 1),
('Campaign 26', 'Description 26', 26000, NOW() + INTERVAL 26 MINUTE, 'active', 2),
('Campaign 27', 'Description 27', 27000, NOW() + INTERVAL 27 MINUTE, 'active', 3),
('Campaign 28', 'Description 28', 28000, NOW() + INTERVAL 28 MINUTE, 'active', 1),
('Campaign 29', 'Description 29', 29000, NOW() + INTERVAL 29 MINUTE, 'active', 2),
('Campaign 30', 'Description 30', 30000, NOW() + INTERVAL 30 MINUTE, 'active', 3),
('Campaign 31', 'Description 31', 31000, NOW() + INTERVAL 31 MINUTE, 'active', 1),
('Campaign 32', 'Description 32', 32000, NOW() + INTERVAL 32 MINUTE, 'active', 2),
('Campaign 33', 'Description 33', 33000, NOW() + INTERVAL 33 MINUTE, 'active', 3),
('Campaign 34', 'Description 34', 34000, NOW() + INTERVAL 34 MINUTE, 'active', 1),
('Campaign 35', 'Description 35', 35000, NOW() + INTERVAL 35 MINUTE, 'active', 2),
('Campaign 36', 'Description 36', 36000, NOW() + INTERVAL 36 MINUTE, 'active', 3),
('Campaign 37', 'Description 37', 37000, NOW() + INTERVAL 37 MINUTE, 'active', 1),
('Campaign 38', 'Description 38', 38000, NOW() + INTERVAL 38 MINUTE, 'active', 2),
('Campaign 39', 'Description 39', 39000, NOW() + INTERVAL 39 MINUTE, 'active', 3),
('Campaign 40', 'Description 40', 40000, NOW() + INTERVAL 40 MINUTE, 'active', 1),
('Campaign 41', 'Description 41', 41000, NOW() + INTERVAL 41 MINUTE, 'active', 2),
('Campaign 42', 'Description 42', 42000, NOW() + INTERVAL 42 MINUTE, 'active', 3),
('Campaign 43', 'Description 43', 43000, NOW() + INTERVAL 43 MINUTE, 'active', 1),
('Campaign 44', 'Description 44', 44000, NOW() + INTERVAL 44 MINUTE, 'active', 2),
('Campaign 45', 'Description 45', 45000, NOW() + INTERVAL 45 MINUTE, 'active', 3),
('Campaign 46', 'Description 46', 46000, NOW() + INTERVAL 46 MINUTE, 'active', 1),
('Campaign 47', 'Description 47', 47000, NOW() + INTERVAL 47 MINUTE, 'active', 2),
('Campaign 48', 'Description 48', 48000, NOW() + INTERVAL 48 MINUTE, 'active', 3),
('Campaign 49', 'Description 49', 49000, NOW() + INTERVAL 49 MINUTE, 'active', 1),
('Campaign 50', 'Description 50', 50000, NOW() + INTERVAL 50 MINUTE, 'active', 2);

CREATE TABLE IF NOT EXISTS CryptoCurrencies (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    symbol VARCHAR(10) NOT NULL,
    exchangeRate DOUBLE NOT NULL,
    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO CryptoCurrencies (name, symbol, exchangeRate) VALUES
('Bitcoin', 'BTC', 50000.00),
('Ethereum', 'ETH', 2000.00),
('Ripple', 'XRP', 1.00),
('Litecoin', 'LTC', 150.00),
('Bitcoin Cash', 'BCH', 300.00);

CREATE TABLE IF NOT EXISTS Donations (
    id INT AUTO_INCREMENT PRIMARY KEY,
    amount DECIMAL(10, 2) NOT NULL,
    cryptoAmount DECIMAL(10, 6) NOT NULL,
    state ENUM('valid', 'invalid') NOT NULL DEFAULT 'valid',
    campaignId INT,
    userId INT,
    cryptoCurrencyId INT,
    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (campaignId) REFERENCES Campaigns(id),
    FOREIGN KEY (userId) REFERENCES Users(id),
    FOREIGN KEY (cryptoCurrencyId) REFERENCES CryptoCurrencies(id)
);

INSERT INTO Donations (amount, cryptoAmount, state, campaignId, userId, cryptoCurrencyId) VALUES
(50.00, 0.001, 'valid', 1, 4, 1),
(75.00, 0.0375, 'valid', 2, 4, 2),
(100.00, 0.002, 'valid', 3, 4, 1),
(150.00, 0.075, 'valid', 4, 4, 2),
(200.00, 0.004, 'valid', 5, 4, 1),
(250.00, 0.125, 'valid', 6, 4, 2),
(300.00, 0.006, 'valid', 7, 4, 1),
(350.00, 0.175, 'valid', 8, 4, 2),
(400.00, 0.008, 'valid', 9, 4, 1),
(450.00, 0.225, 'valid', 10, 4, 2),
(500.00, 0.010, 'valid', 11, 4, 1),
(550.00, 0.275, 'valid', 12, 4, 2),
(600.00, 0.012, 'valid', 13, 4, 1),
(650.00, 0.325, 'valid', 14, 4, 2),
(700.00, 0.014, 'valid', 15, 4, 1),
(750.00, 0.375, 'valid', 16, 4, 2),
(800.00, 0.016, 'valid', 17, 4, 1),
(850.00, 0.425, 'valid', 18, 4, 2),
(900.00, 0.018, 'valid', 19, 4, 1),
(950.00, 0.475, 'valid', 20, 4, 2);


UPDATE Campaigns c
JOIN (
  SELECT campaignId, SUM(amount) as totalRaised
  FROM Donations
  GROUP BY campaignId
) d ON c.id = d.campaignId
SET c.raised = d.totalRaised;
